const InputProps = {
    props: {
        id: {
            type: String,
            required: false,
            default: "animate-input-id"
        },
        name: {
            type: String,
            required: false,
            default: "animate-input-name"
        },
        labelText: {
            type: String,
            required: false,
            default: "Label"
        },
        type: {
            type: String,
            required: false,
            default: "text"
        },
        placeholder: {
            type: String,
            required: false,
            default: ""
        },
        autocomplete: {
            type: String,
            required: false,
            default: "off"
        },
        icon: {
            type: Object,
            required: false,
            default: {
                need: true,
                icon: "user-alt"
            }
        },
    },
}
export default InputProps;