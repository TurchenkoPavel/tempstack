import { createApp } from 'vue'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserAlt, faLock, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserAlt, faLock, faEye, faEyeSlash)

createApp(App)
    .component("fa", FontAwesomeIcon)
    .mount('#app')
